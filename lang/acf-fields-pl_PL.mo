��          <      \       p      q   0   y     �   �  �     s  E   |  (  �                   Active? Before editing please sync fields. %sSync now%s. The variable <code>max_input_vars</code> in the server configuration settings have the value <code>%d</code> - might cause <strong>lose your data</strong> when the website is too big which has too many fields defined by Advanced Custom Fields PRO. Please increase to <code>%d</code>. Project-Id-Version: ACF Flexible Content
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-20 17:23+0200
Last-Translator: Sebastian Pisula <sebastian.pisula@gmail.com>
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-Basepath: ../src
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Generator: Poedit 3.0.1
X-Poedit-SearchPath-0: .
 Aktywny? Przed edycją należy zsynchronizować pola. %sZsynchronizuj teraz%s. Zmienna <code>max_input_vars</code> w ustawieniach konfiguracji serwera ma wartość <code>%d</code> - może spowodować <strong>utratę danych</strong>, gdy strona jest zbyt duża i ma zbyt wiele pól zdefiniowanych w Advanced Gustom Fields PRO. Proszę zwiększyć wartość do <code>%d</code>. 