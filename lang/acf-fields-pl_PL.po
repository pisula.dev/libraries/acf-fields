msgid ""
msgstr ""
"Project-Id-Version: ACF Flexible Content\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-20 17:22+0200\n"
"PO-Revision-Date: 2022-06-20 17:23+0200\n"
"Last-Translator: Sebastian Pisula <sebastian.pisula@gmail.com>\n"
"Language-Team: \n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 "
"|| n%100>14) ? 1 : 2);\n"
"X-Poedit-Basepath: ../src\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-SearchPath-0: .\n"

#: ActiveField.php:33
msgid "Active?"
msgstr "Aktywny?"

#: BlockingEditingFields.php:33
#, php-format
msgid "Before editing please sync fields. %sSync now%s."
msgstr "Przed edycją należy zsynchronizować pola. %sZsynchronizuj teraz%s."

#: WarningLimitVars.php:18
#, php-format
msgid ""
"The variable <code>max_input_vars</code> in the server configuration "
"settings have the value <code>%d</code> - might cause <strong>lose your "
"data</strong> when the website is too big which has too many fields defined "
"by Advanced Custom Fields PRO. Please increase to <code>%d</code>."
msgstr ""
"Zmienna <code>max_input_vars</code> w ustawieniach konfiguracji serwera ma "
"wartość <code>%d</code> - może spowodować <strong>utratę danych</strong>, "
"gdy strona jest zbyt duża i ma zbyt wiele pól zdefiniowanych w Advanced "
"Gustom Fields PRO. Proszę zwiększyć wartość do <code>%d</code>."

#~ msgid "Flexible Content"
#~ msgstr "Elastyczna treść"

#~ msgid "Add section"
#~ msgstr "Dodaj sekcję"

#~ msgid "Predefined Section"
#~ msgstr "Predefiniowana sekcja"

#~ msgid "Section"
#~ msgstr "Sekcja"

#, php-format
#~ msgid "Layout %s not exists"
#~ msgstr "Układ %s nie istnieje"

#~ msgid "Section Field"
#~ msgstr "Pole sekcji"

#~ msgid "is equal to"
#~ msgstr "jest równe"

#~ msgid "Sections"
#~ msgstr "Sekcje"

#~ msgid "Templates"
#~ msgstr "Szablony"
