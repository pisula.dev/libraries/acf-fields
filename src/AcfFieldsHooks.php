<?php

namespace IC\Functionality\ACF\Fields;

use IC\Functionality\ACF\Fields\Integration;

class AcfFieldsHooks {
	public function hooks(): void {
		add_filter( 'acf/settings/autoload', '__return_true' );

		( new JsonFiles() )->hooks();
		( new ActiveField() )->hooks();
		( new DuplicateRow() )->hooks();
		( new LoadTranslations() )->hooks();

		( new Integration\Shortcodes() )->hooks();

		add_action( 'acf/init', [ $this, 'register_acf_hooks' ] );
	}

	public function register_acf_hooks(): void {
		( new WarningLimitVars() )->hooks();
		( new WidgetInstanceFields() )->hooks();
		( new BlockingEditingFields() )->hooks();
	}
}
