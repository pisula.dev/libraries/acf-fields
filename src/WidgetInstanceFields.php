<?php

namespace IC\Functionality\ACF\Fields;

use WP_Widget;

class WidgetInstanceFields {

	public function hooks(): void {
		add_filter( 'widget_display_callback', [ $this, 'add_acf_fields' ], 100, 2 );
	}

	/**
	 * Filters the settings for a particular widget instance.
	 *
	 * @param mixed     $instance The current widget instance's settings.
	 * @param WP_Widget $widget   The current widget instance.
	 *
	 * @return array
	 */
	public function add_acf_fields( $instance, WP_Widget $widget ): array {
		$fields = get_fields( 'widget_' . $widget->id );

		if ( $fields ) {
			$instance = array_merge( $instance, $fields );
		}

		return $instance;
	}
}
