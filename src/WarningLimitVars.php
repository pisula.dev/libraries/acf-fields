<?php

namespace IC\Functionality\ACF\Fields;

class WarningLimitVars {
	private const REQUIRED_MAX_INPUT_VARS = 10000;

	public function hooks(): void {
		add_action( 'admin_notices', [ $this, 'display_input_vars_notice' ] );
	}

	public function display_input_vars_notice(): void {
		$max_input_vars = $this->get_max_input_vars();

		if ( $max_input_vars < $this->get_required_max_input_vars() ) {
			acf_add_admin_notice(
				sprintf(
					__( 'The variable <code>max_input_vars</code> in the server configuration settings have the value <code>%d</code> - might cause <strong>lose your data</strong> when the website is too big which has too many fields defined by Advanced Custom Fields PRO. Please increase to <code>%d</code>.', LoadTranslations::TEXT_DOMAIN ),
					$max_input_vars,
					$this->get_required_max_input_vars()
				),
				'error' );
		}
	}

	/**
	 * @return int
	 */
	private function get_required_max_input_vars(): int {
		return self::REQUIRED_MAX_INPUT_VARS;
	}

	/**
	 * @return int
	 */
	private function get_max_input_vars(): int {
		return (int) ini_get( 'max_input_vars' );
	}
}
