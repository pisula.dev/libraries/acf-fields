<?php

namespace IC\Functionality\ACF\Fields;

class JsonFiles {
	public function hooks(): void {
		add_filter( 'acf/settings/save_json', [ $this, 'save_json' ], 0 );
		add_filter( 'acf/settings/load_json', [ $this, 'load_json' ], 0 );
	}

	/**
	 * @return string
	 */
	public function save_json(): string {
		return $this->get_path();
	}

	/**
	 * @param mixed $paths
	 *
	 * @return array
	 */
	public function load_json( $paths ): array {
		$paths[] = $this->get_path();

		return $paths;
	}

	/**
	 * @return string
	 */
	private function get_path(): string {
		return wp_normalize_path( WP_CONTENT_DIR . '/acf-fields' );
	}
}
