<?php

namespace IC\Functionality\ACF\Fields\Integration;

class Shortcodes {
	public function hooks(): void {
		add_filter( 'acf/format_value/type=textarea', 'do_shortcode' );
		add_filter( 'acf/format_value/type=wysiwyg', 'do_shortcode' );
		add_filter( 'acf/format_value/type=text', 'do_shortcode' );
	}
}
