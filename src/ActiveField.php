<?php

namespace IC\Functionality\ACF\Fields;

class ActiveField {
	private const FIELD_NAME = 'status';

	public function hooks(): void {
		add_filter( 'acf/prepare_field', [ $this, 'acf_prepare_field' ] );
		add_action( 'acf/render_field_settings', [ $this, 'acf_render_field_settings' ] );
	}

	/**
	 * @param mixed $field .
	 *
	 * @return bool|array
	 */
	public function acf_prepare_field( $field ) {
		if ( isset( $field[ self::FIELD_NAME ] ) && ! $field[ self::FIELD_NAME ] ) {
			return false;
		}

		return $field;
	}

	/**
	 * @param array $field
	 */
	public function acf_render_field_settings( array $field ): void {
		acf_render_field_setting(
			$field,
			[
				'label'         => __( 'Active?', LoadTranslations::TEXT_DOMAIN ),
				'instructions'  => '',
				'name'          => self::FIELD_NAME,
				'type'          => 'true_false',
				'default_value' => 1,
				'ui'            => 1,
			],
			true
		);
	}
}
