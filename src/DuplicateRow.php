<?php

namespace IC\Functionality\ACF\Fields;

class DuplicateRow {
	public function hooks(): void {
		add_action( 'acf/input/admin_head', [ $this, 'add_style' ] );

	}

	/**
	 * @return void
	 */
	public function add_style(): void {
		?>
		<style>
			.acf-repeater .acf-row:hover > .acf-row-handle .acf-icon.show-on-shift,
			.acf-repeater .acf-row.-hover > .acf-row-handle .acf-icon.show-on-shift {
				top: auto;
				z-index: 1;
				bottom: -12px;
				display: block !important;
			}
		</style>
		<?php
	}
}
