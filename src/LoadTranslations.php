<?php

namespace IC\Functionality\ACF\Fields;

class LoadTranslations {
	public const TEXT_DOMAIN = 'acf-fields';

	public function hooks(): void {
		add_action( 'init', [ $this, 'load_textdomain' ], 0 );
	}

	public function load_textdomain(): void {
		$mofile = wp_normalize_path( sprintf( '%s/../lang/%s-%s.mo', __DIR__, self::TEXT_DOMAIN, determine_locale() ) );

		load_textdomain( self::TEXT_DOMAIN, $mofile );
	}
}
