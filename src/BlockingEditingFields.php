<?php

namespace IC\Functionality\ACF\Fields;

use ACF_Admin_Field_Groups;

class BlockingEditingFields {

	public function hooks(): void {
		add_action( 'current_screen', [ $this, 'current_screen' ], 100 );
	}

	/**
	 * .
	 */
	public function current_screen(): void {
		if ( ! ic_is_local() ) {
			return;
		}

		if ( ! ( acf_is_screen( 'edit-acf-field-group' ) || acf_is_screen( 'acf-field-group' ) ) ) {
			return;
		}

		/** @var ACF_Admin_Field_Groups $acf_sync */
		$acf_sync = acf_get_instance( 'ACF_Admin_Field_Groups' );

		if ( $acf_sync->sync ) {
			$sync_url = admin_url( 'edit.php' );
			$sync_url = add_query_arg( 'post', array_keys( $acf_sync->sync ), $sync_url );
			$sync_url = add_query_arg( 'action2', 'acfsync', $sync_url );
			$sync_url = add_query_arg( 'post_type', 'acf-field-group', $sync_url );
			$sync_url = wp_nonce_url( $sync_url, 'bulk-posts' );

			wp_die(
				sprintf(
					__( 'Before editing please sync fields. %sSync now%s.', LoadTranslations::TEXT_DOMAIN ),
					'<a href="' . esc_url( $sync_url ) . '">',
					'</a>'
				)
			);
		}
	}
}
