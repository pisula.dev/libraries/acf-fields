## [1.1.1] - 2023-01-17
### Removed
- Orphans integration

## [1.1.0] - 2022-12-07
### Added
- Support duplicate row

## [1.0.1] - 2022-06-21
### Fixed
- check env

## [1.0.0] - 2022-06-20
### Added
- initial version
